## 2015-08-13 Release 0.1.7
### Summary:
- [Beaker] Update Beaker environment
- [RSpec] Update RSpec environment
- [Travis CI] Update Travis CI environment
- [Puppet Forge] Update license, version requirement

## 2015-02-25 Release 0.1.6
### Summary:
- [Beaker] Update Beaker environment
- [Travis CI] Update Travis CI environment

## 2015-02-25 Release 0.1.5
### Summary:
- [Beaker] Update Beaker environment
- [Puppet] Add support for Debian 8.x (Jessie)

## 2014-12-11 Release 0.1.4
### Summary:
- [Puppet] Add missing quotes

## 2014-12-10 Release 0.1.3
### Summary:
- [Puppet] Fix rebuilding *.tar.gz and *.ovpn
- [Puppet] Add support for pushing parameters like route etc.
- [Puppet] Fix default parameters and syntax of the .yaml file

## 2014-12-10 Release 0.1.2
### Summary:
- [Puppet] Change fact for ipaddress

## 2014-12-10 Release 0.1.1
### Summary:
- [Puppet] Add a custom fact because the value of ipaddress gets messed up if you use Docker
- [Puppet] Force rebuilding *.tar.gz and *.ovpn in case of config changes

## 2014-12-08 Release 0.1.0
### Summary:
- Generated from [https://github.com/dhoppe/puppet-skeleton-standard](https://github.com/dhoppe/puppet-skeleton-standard)
